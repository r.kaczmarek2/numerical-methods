// linspace i arange
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

template <typename T>
vector<vector<T>> add(vector<vector<T>> *a, vector<vector<T>> *b)
{
    vector<vector<T>> matrix;
    for (int i = 0; i < (*a).size(); i++)
    {
        vector<T> temp((*a).size());
        matrix.push_back(temp);
        for (int j = 0; j < (*a)[0].size(); j++)
        {
            matrix[i][j] = (*a)[i][j] + (*b)[i][j];
        }
    }
    return matrix;
}

template <typename T>
vector<vector<T>> multiplication(vector<vector<T>> *a, vector<vector<T>> *b)
{
    vector<vector<T>> matrix;

    if ((*a)[0].size() != (*b).size())
    {
        cout << "bledne wymiary macierzy, musi spelniac (m x n)(n x l)";
        return matrix;
    }
    else
    {
        for (int i = 0; i < (*a).size(); i++)
        {
            vector<T> temp((*b)[0].size());
            matrix.push_back(temp);

            for (int j = 0; j < (*b)[0].size(); j++)
            {
                T wynik = 0;
                for (int k = 0; k < (*a)[0].size(); k++)
                {
                    wynik += (*a)[i][k] * (*b)[k][j];
                }

                matrix[i][j] = wynik;
            }
        }
        return matrix;
    }
}

template <typename T>
void print_vec(vector<vector<T>> *a)
{
    for (int i = 0; i < (*a).size(); i++)
    {
        cout << '[';
        for (int j = 0; j < (*a)[0].size(); j++)
        {
            cout << (*a)[i][j];
            if (j + 1 < (*a)[0].size())
            {
                cout << ' ';
            }
        }
        cout << ']' << endl;
    }
}

int main()
{
    vector<vector<float>> v1{{-2, -3, 1}, {-1, 4, 0}};
    vector<vector<float>> v2{{-2, -1, 2}, {3, 0, 1}, {2, 2, -1}};
    vector<vector<float>> v3{{5, 5, 5}, {5, 5, 5}, {5, 5, 5}};
    vector<vector<float>> v6{{1, 1}, {1, 1}, {1, 1}};

    vector<vector<float>> v4 = add(&v2, &v3);
    vector<vector<float>> v5 = multiplication(&v1, &v2);
    vector<vector<float>> v7 = multiplication(&v1, &v6);

    print_vec(&v1);
    cout << endl;
    print_vec(&v4);
    cout << endl;
    print_vec(&v5);
    cout << endl;
    print_vec(&v7);
    return 0;
}