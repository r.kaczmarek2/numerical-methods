// linspace i arange
#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<float> linspace(float start, float end, float num)
{
    vector<float> wektor;
    float krok = (end - start) / (num - 1);
    for (int i = 0; i < num; i++)
    {
        wektor.push_back(start + i * krok);
    }
    return wektor;
}
vector<float> arange(float start, float num, float incr)
{
    vector<float> wektor;
    for (int i = 0; i < num; i++)
    {
        wektor.push_back(start + i * incr);
    }
    return wektor;
}
int main()
{
    float a = 1;
    float b = 2;
    float c = 10;

    vector<float> v1 = arange(10, 10, 1);

    cout << "Wektor size = " << v1.size() << endl;
    for (auto i = 0; i < v1.size(); i++)
    {
        cout << "element " << i + 1 << " = " << v1[i] << endl;

        return 0;
    }
}