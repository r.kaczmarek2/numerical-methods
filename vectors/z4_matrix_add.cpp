// linspace i arange
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

vector<vector<float>> add(vector<vector<float>> a, vector<vector<float>> b)
{
    vector<vector<float>> matrix;
    for (int i = 0; i < a.size(); i++)
    {
        vector<float> temp(a.size());
        matrix.push_back(temp);
        for (int j = 0; j < a.size(); j++)
        {
            matrix[i][j] = a[i][j] + b[i][j];
        }
    }
    return matrix;
}
int main()
{
    vector<vector<float>> v1{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
    vector<vector<float>> v2{{2, 2, 2}, {2, 2, 2}, {2, 2, 2}};
    vector<vector<float>> v3 = add(v1, v2);
    int n = 1;
    for (auto i = 0; i < v3.size(); ++i)
    {
        for (auto j = 0; j < v3.size(); j++)
        {
            cout << "Matrix element " << n << " = " << v3[i][j] << endl;
            n += 1;
        }
    }

    return 0;
}