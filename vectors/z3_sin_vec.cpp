// linspace i arange
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

float sinx(float x)
{
    int n = 1;
    float f0 = x;
    float f1 = x;
    float epsi = 0.001;

    while (fabs(f1) > epsi)
    {
        f1 *= ((-1) * x * x / ((n + 2) * (n + 1)));
        f0 += f1;
        n += 2;
    }
    return f0;
}

vector<float> sinw(vector<float> x)
{
    vector<float> wektor;
    for (int i = 0; i < x.size(); i++)
    {
        wektor.push_back(sinx(x[i]));
    }
    return wektor;
}
int main()
{
    vector<float> a = {1, 2, 3, 4, 5};
    vector<float> v1 = sinw(a);
    for (auto i = 0; i < v1.size(); ++i)
        cout << "Sinus nasz = " << v1[i] << " Sinus z biblioteki " << sin(a[i]) << endl;
    return 0;
}