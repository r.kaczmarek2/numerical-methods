#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include <sstream>
#include "func.h"
#include <numeric>
#include <algorithm>
using namespace std;

struct Wynik
{
    int wart;
    char name;
};
bool operator<(const Wynik &lhs, const Wynik &rhs)
{
    return lhs.wart < rhs.wart;
}

void sortowanie(int tab[], int n)
{
    for (int i = 0; i < n; i++)
        for (int j = 1; j < n - i; j++)
            if (tab[j - 1] > tab[j])
                swap(tab[j - 1], tab[j]);
}

template <typename T>
void sortowanie_vec(vector<T> *vec)
{
    vector<int> ch_list(26, 0);
    iota(ch_list.begin(), ch_list.end(), ch_list.begin());
    stable_sort(ch_list.begin(), ch_list.end(),
                [&vec](int i1, int i2)
                { return vec[i1] < vec[i2]; });
}

int main()
{
    string filename = "text_histogram.txt";
    ifstream data_file(filename);

    if (!data_file)
    {
        cout << "no file " << filename << endl;
        return -1;
    }

    string line;
    char ch;

    vector<int> data_vector(26, 0);
    while (getline(data_file, line))
    {
        stringstream ss(line);

        while (ss >> ch)
        {
            if (ch > 64 && ch < 97)
            {
                ch = char(32 + ch);
            }
            ch = char(ch - 97);
            data_vector[ch]++;
        }
    }

    vector<Wynik> v;
    Wynik temp;
    for (int i = 0; i < 26; i++)
    {

        cout << char(i + 97) << ' ' << data_vector[i] << ' ';
        for (int j = 0; j < (data_vector[i] / 10); j++)
        {
            cout << '*';
        }
        
        temp = {
            data_vector[i],
            char(i + 97),
        };
        v.push_back(temp);
        cout << endl;
    }

    stable_sort(v.begin(), v.end());

    for (int i = 0; i < 5; i++)
        cout << endl;
    for (const Wynik &e : v)
        cout << e.wart << ", " << e.name << '\n';

    return 0;
}
