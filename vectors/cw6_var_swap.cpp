#include <iostream>
#include <string>

using namespace std;

template <typename T>
void swap_t(T *a, T *b)
{
    T temp = *a;
    *a = *b;
    *b = temp;
}

int main()
{
    float a = 5;
    float b = 10;

    swap_t<float>(&a, &b);

    cout << a << " " << b << endl;

    return 0;
}