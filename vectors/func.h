#include <iostream>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

namespace r_math
{
    template <typename T>
    vector<vector<T>> add(vector<vector<T>> *a, vector<vector<T>> *b)
    {
        vector<vector<T>> matrix;
        for (int i = 0; i < (*a).size(); i++)
        {
            vector<T> temp((*a).size());
            matrix.push_back(temp);
            for (int j = 0; j < (*a)[0].size(); j++)
            {
                matrix[i][j] = (*a)[i][j] + (*b)[i][j];
            }
        }
        return matrix;
    }

    template <typename T>
    vector<vector<T>> multiplication(vector<vector<T>> *a, vector<vector<T>> *b)
    {
        vector<vector<T>> matrix;

        if ((*a)[0].size() != (*b).size())
        {
            cout << "bledne wymiary macierzy, musi spelniac (m x n)(n x l)";
            return matrix;
        }
        else
        {
            for (int i = 0; i < (*a).size(); i++)
            {
                vector<T> temp((*b)[0].size());
                matrix.push_back(temp);

                for (int j = 0; j < (*b)[0].size(); j++)
                {
                    T wynik = 0;
                    for (int k = 0; k < (*a)[0].size(); k++)
                    {
                        wynik += (*a)[i][k] * (*b)[k][j];
                    }

                    matrix[i][j] = wynik;
                }
            }
            return matrix;
        }
    }

    template <typename T>
    void print_vec(vector<vector<T>> *a)
    {
        for (int i = 0; i < (*a).size(); i++)
        {
            cout << '[';
            for (int j = 0; j < (*a)[0].size(); j++)
            {
                cout << (*a)[i][j];
                if (j + 1 < (*a)[0].size())
                {
                    cout << ' ';
                }
            }
            cout << ']' << endl;
        }
    }
}