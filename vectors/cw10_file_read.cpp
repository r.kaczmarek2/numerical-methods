#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include <sstream>
#include "func.h"
using namespace std;

int main()
{
    string filename = "Data_set_1.dat";
    ifstream data_file(filename);

    if (!data_file)
    {
        cout << "no file " << filename << endl;
        return -1;
    }

    string line;
    float number;

    vector<vector<float>> data_vector;
    while (getline(data_file, line))
    // for (auto i = 0; i < 5; i++)
    {
        // getline(data_file, line);
        stringstream ss(line);
        // cout << '[';
        vector<float> temp;

        while (ss >> number)
        {
            // cout << number << '\t';
            temp.push_back(number);
        }
        data_vector.push_back(temp);
    }

    cout << data_vector.size() << endl;
    cout << data_vector[0].size() << endl;
    // r_math::print_vec(&data_vector);

    ofstream ofs("new_file.txt"); // nazwa ofs'a i nazwa pliku do ktorego wklada
    ofs << "Nazywam sie Rafal" << endl;
    float a = 5.2313151;
    ofs << a*a*a << endl;
    data_file.close();
    return 0;
}