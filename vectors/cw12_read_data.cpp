#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include <sstream>
#include "func.h"

using namespace std;

template <typename T>
vector<vector<T>> wczytaj(string filename, T typ)
{ // wczytuje vectory we float
    ifstream data_file(filename);
    if (!data_file)
    {
        cerr << "no file " << filename << endl;
    }

    string line;
    T number = typ;

    vector<vector<T>> data_vector;
    while (getline(data_file, line))
    {
        stringstream ss(line);
        vector<T> temp;

        while (ss >> number)
        {
            temp.push_back(number);
        }
        data_vector.push_back(temp);
    }
    data_file.close();
    cout << "size_0 " << data_vector.size() << endl;
    cout << "size_1 " << data_vector[0].size() << endl;

    return data_vector;
}
int main()
{

    string filename1 = "Data_set_1.dat";
    string filename2 = "Data_set_2.dat";

    float typ;

    vector<vector<float>> data_vector1 = wczytaj(filename1, typ);
    vector<vector<float>> data_vector2 = wczytaj(filename2, typ);
    vector<vector<float>> v4;
    // vector<vector<float>> v4 = r_math::add(&data_vector1, &data_vector2);

    for (int i = 0; i < data_vector1.size(); i++)
    {
        for (int j = 1; j < data_vector1[i].size(); j++)
        {
            v4[i][j] += data_vector1[i][j];
        }
        
    }

    ofstream ofs("cw12.txt");

    for (int i = 0; i < (v4).size(); i++)
    {
        for (int j = 0; j < (v4)[0].size(); j++)
        {
            ofs << v4[i][j] << '\t';
        }
        ofs << endl;
    }
    return 0;
}