// linspace i arange
#include <iostream>
#include <string>
#include <vector>

using namespace std;

float skalar(vector<float> v1, vector<float> v2)
{
    float suma = 0;
    if (v1.size() == v2.size())
    {
        for (int i = 0; i < v1.size(); i++)
        {
            suma += v1[i] * v2[i];
        }
    }
    else
    {
        cout << "To nie zadziala";
        return -1;
    }
    return suma;
}

vector<float> prosta(vector<float> v1, vector<float> v2)
{
    vector<float> wektor(v1);
    for (int i = 0; i < v2.size(); i++)
    {
        wektor.push_back(v2[i]);
    }
    return wektor;
}

vector<float> tensor(vector<float> v1, vector<float> v2)
{
    vector<float> wektor;
    for (int i = 0; i < v1.size(); i++)
    {
        for (int j = 0; j < v2.size(); j++)
        {
            wektor.push_back(v1[i] * v2[j]);
        }
    }
    return wektor;
}

int main()
{
    vector<float> a = {1, 2, 3};
    vector<float> b = {4, 5, 6};

    vector<float> v1 = prosta(a, b);
    for (auto i = 0; i < v1.size(); i++)
    {
        cout << "element " << i + 1 << " = " << v1[i] << endl;
    }

    return 0;
}