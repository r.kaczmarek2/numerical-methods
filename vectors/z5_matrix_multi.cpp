// linspace i arange
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

vector<vector<float>> multiplication(vector<vector<float>> a, vector<vector<float>> b)
{
    vector<vector<float>> matrix;

    if (a[0].size() != b.size())
    {
        cout << "bledne wymiary macierzy, musi spelniac (m x n)(n x l)";
        return matrix;
    }
    else
    {
        for (int i = 0; i < a.size(); i++)
        {
            vector<float> temp(a[0].size());
            matrix.push_back(temp);

            for (int j = 0; j < a[0].size(); j++)
            {
                float wynik = 0;
                for (int k = 0; k < a[0].size(); k++)
                {
                    wynik += a[i][k] * b[k][j];
                }

                matrix[i][j] = wynik;
            }
        }
        return matrix;
    }
}
int main()
{
    vector<vector<float>> v1{{-2, -3, 1}, {-1, 4, 0}};
    vector<vector<float>> v2{{-2, -1, 2}, {3, 0, 1}, {2, 2, -1}};
    vector<vector<float>> v3 = multiplication(v1, v2);
    int n = 1;
    for (auto i = 0; i < v3.size(); ++i)
    {
        for (auto j = 0; j < v3[0].size(); j++)
        {
            cout << "Matrix element " << n << " = " << v3[i][j] << endl;
            n += 1;
        }
    }

    return 0;
}