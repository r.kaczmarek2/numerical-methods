// linspace i arange
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include "func.h"

using namespace std;
// using namespace r_math;

int main()
{
    vector<vector<float>> v1{{-2, -3, 1}, {-1, 4, 0}};
    vector<vector<float>> v2{{-2, -1, 2}, {3, 0, 1}, {2, 2, -1}};
    vector<vector<float>> v3{{5, 5, 5}, {5, 5, 5}, {5, 5, 5}};
    vector<vector<float>> v6{{1, 1}, {1, 1}, {1, 1}};

    vector<vector<float>> v4 = r_math::add(&v2, &v3);
    vector<vector<float>> v5 = r_math::multiplication(&v1, &v2);
    vector<vector<float>> v7 = r_math::multiplication(&v1, &v6);

    r_math::print_vec(&v1);
    cout << endl;
    r_math::print_vec(&v4);
    cout << endl;
    r_math::print_vec(&v5);
    cout << endl;
    r_math::print_vec(&v7);
    return 0;
}