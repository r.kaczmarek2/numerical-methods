// linspace i arange
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

template <typename T>
void print_vec(vector<vector<T>> *a)
{
    for (int i = 0; i < (*a).size(); i++)
    {
        cout << '[';
        for (int j = 0; j < (*a)[0].size(); j++)
        {
            cout << (*a)[i][j];
            if (j + 1 < (*a)[0].size())
            {
                cout << ' ';
            }
        }
        cout << ']' << endl;
    }
}
int main()
{
    vector<vector<float>> v1{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {10, 11, 12}};

    print_vec(&v1);
    return 0;
}