#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
using namespace std;

class Osoba
{
    string imie;
    int tel;

public:
    Osoba()
    {
        imie = "";
        tel = 0;
    }
    Osoba(string _i)
    {
        imie = _i;
        tel = 0;
    }
    Osoba(string _i, int _t)
    {
        imie = _i;
        tel = _t;
    }
    Osoba(const Osoba &_O)
    {
        imie = _O.imie;
        tel = _O.tel;
    }
    void ustawImie(string _i = "NN") { imie = _i; }
    void ustawTel(int _t = 0) { tel = _t; }
    string podajImie() const { return imie; }
    int podajTel() const { return tel; }
    bool operator<(Osoba &_O2); /* Tu prototypy, implementacje ponizej */
    void Wypisz();
};

bool Osoba::operator<(Osoba &_O2)
{
    if (imie.compare(_O2.imie) < 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Osoba::Wypisz()
{
    cout << "imie: " << imie << "tel: " << tel;
}

typedef vector<Osoba> Vo;

class Notatnik
{
    Vo Dane;

public:
    Notatnik() {}
    Notatnik(const Osoba &_Os) { Dane = Vo{_Os}; }
    Notatnik(const Vo &_Dane) { Dane = _Dane; }
    Notatnik(const Notatnik &_N) { Dane = _N.Dane; }
    Vo podajDane() const { return Dane; }
    void ustawDane(Vo _D) { Dane = _D; }
    void Wypisz();
    Notatnik &operator+=(Osoba &_Os); /* Tu prototypy, implementacje ponizej */
    Notatnik &operator+=(Notatnik &_N2);
    Notatnik &operator-=(Osoba &_Os);
    Notatnik &SortAlfa();
};

Notatnik &Notatnik::SortAlfa()
{
	for(int i=0;i<Dane.size();i++)
		for(int j=1;j<Dane.size()-i;j++) 
		if(Dane[j].podajImie()<Dane[j-1].podajImie())
			swap(Dane[j-1], Dane[j]);

    return *this; // this = wskaznik do obiektu rodzimego. *this to sam obiekt rodzimy.
}

Notatnik &Notatnik::operator+=(Osoba &_Os)
{
    Dane.push_back(_Os);
    return *this;
}

Notatnik &Notatnik::operator+=(Notatnik &_N2)
{
    for (int i = 0; i < Dane.size(); i++)
    {
        Dane.push_back(_N2.Dane[i]);
    }
    return *this;
}

Notatnik &Notatnik::operator-=(Osoba &_Os)
{
    for (int i = 0; i < Dane.size(); i++)
    {

        if (Dane[i].podajImie() == _Os.podajImie())
        {
            Dane.erase(Dane.begin() + i);
        }
    }
    return *this;
}

void Notatnik::Wypisz()
{
    for (int i = 0; i < Dane.size(); i++)
    {
        cout << Dane[i].podajImie() << " " << Dane[i].podajTel() << endl;
    }
}

int main()
{
    Osoba O1("Natalia", 321654), O2("Janek", 654321),
        O3("Anna", 123456), O4("Staszek", 456123),
        O5("Rafal", 543210), O6("Agata", 432105);
    Vo RawData = {O1, O2, O3, O4, O5};

    Notatnik N(RawData);

    N += O6;
    N -= O4;

    N.SortAlfa();
    N.Wypisz();

    return 0;
}