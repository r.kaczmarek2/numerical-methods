#ifndef __matrix__
#define __matrix__

#include <iostream>
#include <vector>
#include <iomanip>
#include <string>
using namespace std;

template <class T1>
struct matrix
{
    int nr;
    int nc;
    vector<vector<T1>> data;
    matrix();
    matrix(T1);
    matrix(int _nr, int _nc);
    matrix(T1 a, T1 b, T1 c, T1 d);
    matrix(int _nr, int _nc, vector<vector<T1>> &v0);
    matrix(const matrix &m0);
    ~matrix();

    matrix &operator+=(T1 x);
    matrix &operator+=(matrix &M0);
    matrix &operator*=(T1 x);
    matrix operator-();
    matrix operator+(T1 x);
    matrix operator+(matrix &M1);
    void print();
    matrix product(matrix &A, matrix &B);
    matrix Transpose(matrix &M0);
};

#include "Matrix_class.C"
#endif