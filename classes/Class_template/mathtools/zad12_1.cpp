#include <iostream>
#include <cmath>
#include <vector>
#include <limits>
#include <string>
#include "MTFun.h"
#include "mathtools.h"

//#include <matplot/matplot.h>

int main()
{
    double xmin = 0;
    double xmax = 6.28;
    Function<double> s(sin, xmin, xmax);
    std::cout << "wartosc funkcji " << s(0) << std::endl;
    std::cout << "calka " << MathTools<Function<double>, double>::Integral(s, 1000) << std::endl;
    std::cout << "Root " << MathTools<Function<double>, double>::Root(s, 2, 4) << std::endl;
    std::cout << "cout eps " << MathTools<Function<double>, double>::eps << std::endl;

    // MathTools<Function<double>,double>::Plotter(s,0,10,1000,true);
    return 0;
}