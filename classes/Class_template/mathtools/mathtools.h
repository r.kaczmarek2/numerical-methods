#ifndef __FUN_math__
#define __FUN_math__

#include <iostream>
#include <cmath>
#include <vector>
#include <limits>


template<class T,class T1>
class MathTools
{
    public:
    static constexpr T1 eps = std::numeric_limits<T1>::epsilon()*10000;
    static T1 Integral(T &F, int n);
    static T1 Root(T &F, T1 x1, T1 x2);
    static void Plotter(T &F, T1 xmin, T1 xmax, int Npoints, bool block);
};

#include "mathtools.C"
#endif