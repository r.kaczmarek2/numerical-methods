#ifndef __FUN_1math__
#define __FUN_1math__

#include <iostream>
#include <cmath>
#include <vector>
#include <limits>

template <class T>
class Function
{
public:
    T(*myfun)
    (T);
    T xmin;
    T xmax;

    Function(T (*_myfun)(T), T _xmin, T _xmax)
    {
        myfun = _myfun;
        xmin = _xmin;
        xmax = _xmax;
    }
    T operator()(T x)
    {
        return myfun(x);
    }
};

#endif