#include <iostream>
#include <vector>
#include <iomanip>
#include <string>
#include "Matrix_class.h"
using namespace std;

template <class T1,class T2>
matrix<T1,T2>::matrix()
{
    data = vector<vector<T1>>{{0, 0}, {0, 0}};
    nc = 2;
    nr = 2;
}

template <class T1, class T2>
matrix<T1,T2>::matrix(T2 _nr, T2 _nc)
{
    data = vector<vector<T1>> (_nr, vector<T1>(_nc, 0));
    nc = _nc;
    nr = _nr;
}

template <class T1, class T2>
matrix<T1,T2>::matrix(T1 a, T1 b, T1 c, T1 d, T2 e)
{
    T2 nc = 2;
    T2 nr = 2;
    data = vector<vector<T1>>{{a, b}, {c, d}};
}
template <class T1, class T2>
matrix<T1,T2>::matrix(T2 _nr, T2 _nc, vector<vector<T1>> &m0)
{
    nr = _nr;
    nc = _nc;
    data = vector<vector<T1>(_nr, vector<T1>(_nc, 0));
    for (T1 i = 0; i < _nr; i++)
    {
        for (T1 j = 0; j < _nc; j++)
        {
            data[i][j] = m0[i][j];
        }
    }
}

template <class T1, class T2>
matrix<T1,T2>::matrix(const matrix &m0)
{
    nr = m0.nr;
    nc = m0.nc;
    data = m0.data;
}

template <class T1, class T2>
matrix<T1,T2>::~matrix()
{
    cout << "niszczy" << endl;
}
template <class T1, class T2>
matrix<T1,T2> &matrix<T1,T2>::operator+=(T1 x)
{
    for (T1 i = 0; i < data.size(); i++)
    {
        for (T1 j = 0; j < data[0].size(); j++)
        {
            data[i][j] += x;
        }
    }
    return *this;
}

template <class T1, class T2>
matrix<T1,T2> &matrix<T1,T2>::operator+=(matrix<T1,T2> &M2)
{
    if (nc == M2.nc && nr == M2.nr)
    {
        for (T1 i = 0; i < data.size(); i++)
        {
            for (T1 j = 0; j < data[0].size(); j++)
            {
                data[i][j] += M2.data[i][j];
            }
        }
        return *this;
    }
    else
    {
        cout << "rozne rozmiary macierzy" << endl;
        exit(-1);
    }
}

template <class T1, class T2>
matrix<T1,T2> &matrix<T1,T2>::operator*=(T1 x)
{
    for (T1 i = 0; i < data.size(); i++)
    {
        for (T1 j = 0; j < data[0].size(); j++)
        {
            data[i][j] *= x;
        }
    }

    return *this;
}

template <class T1, class T2>
matrix<T1,T2> matrix<T1,T2>::operator-()
{
    vector<vector<T1> vec(T2 nr, vector<T1>(nc, 0));
    for (T1 i = 0; i < data.size(); i++)
    {
        for (T1 j = 0; j < data[0].size(); j++)
        {
            vec[i][j] = -data[i][j];
        }
    }

    return matrix(nr, nc, vec);
}

template <class T1, class T2>
matrix<T1,T2> matrix<T1,T2>::operator+(T1 x)
{
    vector<vector<T1> vec(nr, vector<T1>(nc, 0));
    for (T1 i = 0; i < data.size(); i++)
    {
        for (T1 j = 0; j < data[0].size(); j++)
        {
            vec[i][j] = data[i][j] + x;
        }
    }

    return matrix(nr, nc, vec);
}

template <class T1, class T2>
matrix<T1,T2> matrix<T1,T2>::operator+(matrix<T1,T2> &m1)
{
    vector<vector<T1> vec(nr, vector<T1>(nc, 0));
    if (nc == m1.nc && nr == m1.nr)
    {
        for (T1 i = 0; i < nr; i++)
        {
            for (T1 j = 0; j < nc; j++)
            {
                vec[i][j] = data[i][j] + m1.data[i][j];
            }
        }
        return matrix(nr, nc, vec);
    }
    else
    {
        cout << "rozne rozmiary macierzy" << endl;
        exit(-1);
    }
}

template <class T1, class T2>
void matrix<T1,T2>::print()
{
    for (T1 ir = 0; ir < nr; ir++)
    {
        for (T1 ic = 0; ic < nc; ic++)
        {
            cout << setw(10) << data[ir][ic];
        }
        cout << endl;
    }
    cout << endl;
}

/*** Funkcje poza klasa ***/

template <class T1, class T2>
matrix<T1,T2> operator+(T1 x, matrix<T1,T2> m0)
{ // Uwaga: Przyjmowanie kopii. Celowo.

    for (T1 i = 0; i < m0.data.size(); i++)
    {
        for (T1 j = 0; j < m0.data[0].size(); j++)
        {
            m0.data[i][j] += x;
        }
    }
    return m0;
}

template <class T1, class T2>
matrix<T1,T2> matrix<T1,T2>::product(matrix<T1,T2> &A, matrix<T1,T2> &B)
{
    vector<vector<T1> result;
    if (A.nc != B.nr)
    {
        cout << "bledne wymiary macierzy, musi spelniac (m x n)(n x l)";
        cout << "Wymiary A.nc = " << A.nc << "wymiary B.nr = " << B.nr << endl;
        exit(-1);
    }
    else
    {
        for (T1 i = 0; i < A.nr; i++)
        {
            vector<T1> temp(A.nr);
            result.push_back(temp);

            for (T1 j = 0; j < B.nc; j++)
            {
                T1 wynik = 0;
                for (T1 k = 0; k < A.nr; k++)
                {
                    wynik += A.data[i][k] * B.data[k][j];
                }

                result[i][j] = wynik;
            }
        }
        return matrix(result.size(), result[0].size(), result);
    }
}

template <class T1, class T2>
matrix<T1,T2> matrix<T1,T2>::Transpose(matrix<T1,T2> &M0)
{
    vector<vector<T1> result(M0.nc, vector<T1>(M0.nr, 0));
    for (T1 i = 0; i < M0.nc; i++)
    {
        for (T1 j = 0; j < M0.nr; j++)
        {
            result[j][i] = M0.data[i][j];
        }
    }
    matrix f_result(M0.nc, M0.nr, result);
    return f_result;
}