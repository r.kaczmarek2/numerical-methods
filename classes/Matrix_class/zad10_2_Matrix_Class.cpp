#include <iostream>
#include <vector>
#include <iomanip>
#include <string>
#include "Matrix_class.h"
using namespace std;

int main()
{

    /*** Testing multiplication of two ID_2x2 matrices ***/
    
    matrix A(1.0, 0.0, 0.0, 1.0,1);
    matrix B(2, 2, A.data);

    cout << A.nr << A.nc << endl;
    matrix AB = A.product(A, B);
    cout << endl
         << "Id_2x2 * Id_2x2: " << endl;
    AB.print();

    cout << endl;

    /*** Testing multiplication of two other 2x2 matrices, and transposition of result***/

    matrix C(1, 2, 3, 4,1);
    matrix D(5, 6, 7, 8,1);
    matrix CD = C.product(C, D);

    cout << "For 2-dim matrices C,D, printing out C*D: " << endl;
    CD.print();

    cout << endl
         << "For C*D, printing out Transpose(C*D): " << endl;
    matrix CDtrans = C.Transpose(CD);
    CDtrans.print();
    cout << endl;

    /*** Testing multiplication of 3x3 matrices ***/

    vector<vector<double>> data3(3);
    for (int ir = 0; ir < 3; ir++)
    {
        data3[ir].resize(3);
        for (int ic = 0; ic < 3; ic++)
        {
            data3[ir][ic] = (ir == ic) ? 1 : 0;
        }
    }

    matrix E(3, 3, data3);
    E.print();

    matrix F(E.nr, E.nc, E.data);
    E.print();

    cout << "For two above 3-dim matrices E, F, printing out E*F: " << endl;
    matrix EF = E.product(E, F);
    EF.print();
    cout << endl;

    /*** Testing operators +, - ***/

    cout << "[G] " << endl;
    matrix G(1, 2, 3, 4,1);
    G.print();

    cout << "[G+1] " << endl;
    matrix H = G + 1.;
    H.print();

    cout << "[-G] " << endl;
    matrix J = -G;
    J.print();

    cout << "[G + (-G)] " << endl;
    matrix K = G + J;
    K.print();

    cout << endl;

    /*** Testing scalar product of vec1^T * vec2 ***/
    vector<vector<double>> data_vec4(4);
    for (int ir = 0; ir <= 3; ir++)
    {
        data_vec4[ir].resize(1);
        data_vec4[ir][0] = ir;
    }
    matrix vec4(4, 1, data_vec4);

    vector<vector<double>> data_vec4T(1);
    data_vec4T[0].resize(4);
    for (int ic = 0; ic <= 3; ic++)
    {
        data_vec4T[0][ic] = ic;
    }
    matrix vec4T(1, 4, data_vec4T);

    matrix Mscal = vec4T.product(vec4T, vec4);
    Mscal.print();

    return 0;
}