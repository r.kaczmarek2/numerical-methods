#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <valarray>
#include <vector>
#include <utility>
#include "Regresja2.h"
//#include "matplotlibcpp.h"

using namespace std;

typedef std::valarray<double> valData;
typedef std::vector<double> vecData;


valData Regresja::DxData = {};
valData Regresja::DyData = {};
double Regresja::a_1 = {};
double Regresja::Sa_1 = {};
double Regresja::S_1 = {};

int main(int argc, char *argv[])
{
    valData vM = {13.04, 23.69, 34.32, 44.95, 55.61};
    valData vL = {47.0, 83.5, 122.0, 160.0, 198.0};
    double a = 0.0;
    double S = 0.0;
    double Sa = 0.0;
    Regresja dd(vM, vL,a,S,Sa);

    string file_name = "dane.txt";
    Regresja aa(file_name);

    dd.Analiza();
    dd.Status();

    cout<<"wczytanie z pliku"<<endl;
    aa.Analiza();
    aa.Status();


    return 0;
}