#ifndef __Regresja__
#define __Regresja__

#include <iostream>
#include <fstream>
#include <string>
#include <valarray>
#include <vector>

typedef std::valarray<double> valData;
typedef std::vector<double> vecData;

class Regresja
{
    valData x;
    valData y;
    double a;
    double S;
    double Sa;
    friend std::ostream &operator<<(std::ostream &strumien, const Regresja &Z);

public:
    Regresja &Wczytaj(std::string &inputFileName);
    Regresja(valData _x, valData _y);
    Regresja(std::string &inputFileName);
    valData podaj_x() const;
    valData podaj_y() const;
    double podaj_a() const;
    double podaj_S() const;
    double podaj_Sa() const;

    Regresja &Analiza();
    void Status();
    void ZapiszNaPlik(std::string &OutputFileName);
    void Wyrysuj();
};

#endif