#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <valarray>
#include <vector>
#include <utility>
#include "Regresja.h"
//#include "matplotlibcpp.h"

using namespace std;

int main(int argc, char *argv[])
{
    valData vM = {13.04, 23.69, 34.32, 44.95, 55.61};
    valData vL = {47.0, 83.5, 122.0, 160.0, 198.0};
    double a = 0.0;
    double S = 0.0;
    double Sa = 0.0;
    Regresja dd(vM, vL);
    string outputfile = "output.txt";

    dd.Analiza();
    dd.Status();


    return 0;
}