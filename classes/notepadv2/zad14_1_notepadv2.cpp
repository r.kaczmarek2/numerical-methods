#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <string>
#include <fstream>
#include <cstdlib>
#include <getopt.h>
using namespace std;

class Osoba
{
    string imie;
    int tel;

public:
    Osoba()
    {
        imie = "";
        tel = 0;
    }
    Osoba(string _i)
    {
        imie = _i;
        tel = 0;
    }
    Osoba(string _i, int _t)
    {
        imie = _i;
        tel = _t;
    }
    Osoba(const Osoba &_O)
    {
        imie = _O.imie;
        tel = _O.tel;
    }
    void ustawImie(string _i = "NN") { imie = _i; }
    void ustawTel(int _t = 0) { tel = _t; }
    string podajImie() const { return imie; }
    int podajTel() const { return tel; }
    bool operator<(Osoba &_O2); /* Tu prototypy, implementacje ponizej */
    friend ostream &operator<<(ostream &os, const Osoba &o);
};

bool Osoba::operator<(Osoba &_O2)
{
    if (imie.compare(_O2.imie) < 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}
ostream &operator<<(ostream &os, const Osoba &o)
{
    return os << o.imie << ' ' << o.tel << endl;
}

typedef vector<Osoba> Vo;

class Notatnik
{
    Vo Dane;

public:
    Notatnik() {}
    Notatnik(const Osoba &_Os) { Dane = Vo{_Os}; }
    Notatnik(const Vo &_Dane) { Dane = _Dane; }
    Notatnik(const Notatnik &_N) { Dane = _N.Dane; }
    Notatnik(string filename);
    Vo podajDane() const { return Dane; }
    void ustawDane(Vo _D) { Dane = _D; }
    void Wypisz();
    Notatnik &operator+=(Osoba &_Os); /* Tu prototypy, implementacje ponizej */
    Notatnik &operator+=(Notatnik &_N2);
    Notatnik &operator-=(Osoba &_Os);
    Notatnik &SortAlfa();
    friend ostream &operator<<(ostream &os, const Notatnik &p);
};

Notatnik &Notatnik::SortAlfa()
{
    for (int i = 0; i < Dane.size(); i++)
        for (int j = 1; j < Dane.size() - i; j++)
            if (Dane[j].podajImie() < Dane[j - 1].podajImie())
                swap(Dane[j - 1], Dane[j]);

    return *this; // this = wskaznik do obiektu rodzimego. *this to sam obiekt rodzimy.
}

Notatnik::Notatnik(string filename)
{
    string name;
    int tel;
    ifstream fin(filename);
    Vo data_vec;

    if (!fin.is_open()) // Bezpiecznik, gdyby plik się nie otworzył
    {
        cerr << "Plik nie dal sie otworzyc.\n";
        exit(-1);
    }
    while (fin >> name >> tel)
    {
        Osoba O1(name, tel);
        data_vec.push_back(O1);
    }
    fin.close();
    Dane = data_vec;
}

Notatnik &Notatnik::operator+=(Osoba &_Os)
{
    Dane.push_back(_Os);
    return *this;
}

Notatnik &Notatnik::operator+=(Notatnik &_N2)
{
    for (int i = 0; i < Dane.size(); i++)
    {
        Dane.push_back(_N2.Dane[i]);
    }
    return *this;
}

Notatnik &Notatnik::operator-=(Osoba &_Os)
{
    for (int i = 0; i < Dane.size(); i++)
    {

        if (Dane[i].podajImie() == _Os.podajImie())
        {
            Dane.erase(Dane.begin() + i);
        }
    }
    return *this;
}

ostream &operator<<(ostream &os, const Notatnik &o)
{
    string return_str;
    for (int i = 0; i < o.Dane.size(); i++)
    {
        return_str.append(o.Dane[i].podajImie());
        return_str.append(" ");
        return_str.append(to_string(o.Dane[i].podajTel()));
        return_str.append("\n");
    }
    return os << return_str << endl;
}

Notatnik fixed()
{
    Osoba O1("Natalia", 321654), O2("Janek", 654321),
        O3("Anna", 123456), O4("Staszek", 456123),
        O5("Rafal", 543210), O6("Agata", 432105);
    Vo RawData = {O1, O2, O3, O4, O5};
    Notatnik N(RawData);
    N.SortAlfa();

    return N;
}

Notatnik wczytaj(string filename)
{
    Notatnik N(filename);
    return N;
}

class Osobav2 : public Osoba
{
    string imie;
    int tel;
    string country;

public:
    Osobav2(string _imie, int _tel, string _country) : Osoba(_imie, _tel), country(_country) {}
    Osobav2(string _imie, int _tel) : Osoba(_imie, _tel), country("none") {}
    Osobav2(string _imie) : Osoba(_imie), country("none") {}
    Osobav2() : Osoba(), country("none") {}
    string podajKraj() const { return country; }
    friend ostream &operator<<(ostream &os, const Osoba &o);
};
ostream &operator<<(ostream &os, const Osobav2 &o)
{
    return os << o.podajImie() << ' ' << o.podajTel() << ' ' << o.podajKraj() << endl;
}

class Notatnikv2 : public Notatnik
{
    vector<Osobav2> Dane;

public:
    Notatnikv2() : Notatnik() {}
    Notatnikv2(const Osobav2 &_Os) : Notatnik(_Os) {}

    Notatnikv2(const Notatnikv2 &_N) : Notatnik(_N) {}
    Notatnikv2(vector<Osobav2> _Dane) { Dane = _Dane; }
    Notatnikv2(string filename)
    {
        string name;
        int tel;
        string country;
        ifstream fin(filename);
        vector<Osobav2> data_vec;

        if (!fin.is_open()) // Bezpiecznik, gdyby plik się nie otworzył
        {
            cerr << "Plik nie dal sie otworzyc.\n";
            exit(-1);
        }
        while (fin >> name >> tel >> country)
        {
            Osobav2 O1(name, tel, country);
            data_vec.push_back(O1);
        }
        fin.close();
        Dane = data_vec;
    }
};
int main(int argc, char *argv[])
{

    string filename;
    Notatnik N;

    int test = 0;
    int opt;
    while (true)
    {
        opt = getopt(argc, argv, ":f:gh");

        if (opt == -1)
            break;

        switch (opt) // nie da się definiować zmiennych w switch
        {
        case 'g':
            cout << "generic" << endl;
            N = fixed();
            cout << N;
            break;
        case 'f':
            cout << "Filename: " << optarg << endl;
            N = wczytaj(optarg);
            cout << N;
            break;
        case ':':
            cout << "Potrzebna nazwa pliku" << endl;
            break;
        case 'h':
            cout << "Pomoc: Wpisz -g aby wywolac plik z zafixowanymi danymi, -f <nazwa pliku> aby wczytac z pliku " << endl;
            break;
        }
    }
}
