#include <iostream>
#include <unistd.h>
#include <fstream>
#include <random>
#include <chrono>
#include <iomanip>

// mingw32-make -f makefile test
int main()
{
    double liczba;
    auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed);
    std::uniform_real_distribution<double> losuj{0, 10};

    std::ofstream myStr("output.txt"); // ofstream: strumień plikowy zapisowy.
                                       // Deklaracja od razu z podpięciem do pliku

    if (!myStr.is_open()) // Bezpiecznik, gdyby plik się nie otworzył
    {
        std::cout << "Plik nie dal sie otworzyc.\n";
        return -1;
    }

    while (true)
    {
        for (int i = 0; i < 3; i++)
        {
            liczba = losuj(gen);
            myStr << liczba << ' ';
        }
        myStr << std::endl;

        sleep(2);
        std::streampos myPosOut = myStr.tellp();
        std::cout << myPosOut << std::endl;
        }
    return 0;
}