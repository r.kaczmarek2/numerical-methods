#include <iostream>
#include <vector>

class Funkcja
{
public:
    double xmin, xmax;

    Funkcja(double _xmin, double _xmax)
    {
        xmin = _xmin;
        xmax = _xmax;
    }

    // virtual std::iostream operator<<(Funkcja &) = 0;
    virtual double operator()(double) = 0;
};

class Wielomian : public Funkcja
{
    std::vector<double> par;

public:
    Wielomian(double _xmin, double _xmax, std::vector<double> _par) : Funkcja(_xmin, _xmax), par(_par) {}

    void Status()
    {
        std::cout << "Status" << std::endl;
    }
    double operator()(double x)
    {
        double temp_x = x;
        double temp_wartosc = par[0];

        if (x < xmin || x > xmax)
        {
            throw std::string("argument poza dziedzina");
        }
        else
        {
            for (int i = 1; i < par.size(); i++)
            {
                temp_wartosc += temp_x * par[i];
                temp_x *= x;
            }
        }
        return temp_wartosc;
    }
};

int main()
{
    std::vector<double> A = {3, -4, 2};

    Wielomian W(-10, 10, A);

    double x = -20;
    try{
        W(-x);
    }
    catch (std::string& S){
        std::cerr<<"Wyjate: "<<S<<std::endl;
    }

    std::cout << W(x) << std::endl;
    return 0;
}