#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <cstdlib>

int main()
{

    std::string line;
    std::ifstream fin("output.txt"); // ofstream: strumień plikowy zapisowy.
                                     // Deklaracja od razu z podpięciem do pliku
    std::streampos cur_pos = fin.tellg(), new_pos;
    if (!fin.is_open()) // Bezpiecznik, gdyby plik się nie otworzył
    {
        std::cout << "Plik nie dal sie otworzyc.\n";
        return -1;
    }
    while (true)
    {
        fin.seekg(0, std::ios::end);
        new_pos = fin.tellg();

        if(new_pos!=cur_pos)
        {
            fin.seekg(cur_pos, std::ios::beg);
            std::getline(fin,line);
            cur_pos = new_pos;
            std::cout<<line<<std::endl;

        }
    }

    return 0;
}