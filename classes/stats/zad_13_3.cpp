#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include "matplotlibcpp.h"

namespace plt = matplotlibcpp;

class Rozklad
{
public:
    double T;
    std::string nazwa;
    Rozklad()
    {
        T = 293;
        nazwa = "aa";
    }
    Rozklad(double _T, std::string _nazwa)
    {
        T = _T;
        nazwa = _nazwa;
    }
    virtual double operator()(double) = 0;
};

class Boltzmann : public Rozklad
{
public:
    Boltzmann(double _T, std::string _nazwa)
    {
        T = _T;
        nazwa = _nazwa;
    }

    double operator()(double x) { return exp(-x / T); }
};

class BoseEinstain : public Rozklad
{
public:
    BoseEinstain(double _T, std::string _nazwa)
    {
        T = _T;
        nazwa = _nazwa;
    }

    double operator()(double x) { return 1 / (exp(x / T) - 1); }
};

class FermiDirac : public Rozklad
{
public:
    FermiDirac(double _T, std::string _nazwa)
    {
        T = _T;
        nazwa = _nazwa;
    }

    double operator()(double x) { return 1 / (exp(x / T) + 1); }
};

void Rysuj_mpl(Rozklad &R, double xmin, double xmax, double dx, std::string name="rysunek.png")
{
    int siz = int((xmax - xmin) / dx);
    std::vector<double> X(siz);
    std::vector<double> Y(siz);
    double temp_x;

    for (int i = 0; i < X.size(); i++)
    {
        temp_x = i * dx;
        X[i] = temp_x;
        Y[i] = R(temp_x);
    }

  plt::plot (X, Y);
  plt::show();
  plt::save(name);

    
}
int main()
{
    Boltzmann A(10, "boltz");
    BoseEinstain B(10, "BoseEinstain");
    FermiDirac C(10, "FermiDirac");
    std::cout << A(5) << std::endl;
    std::cout << B(5) << std::endl;
    std::cout << C(5) << std::endl;

  
    Rysuj_mpl(C,0.2,20,0.1,"Rysunek.png");
    return 0;
}