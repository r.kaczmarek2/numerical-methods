#include <iostream>
#include <string>
#include <vector>
using namespace std;

template <typename T>
T myfunc(T x)
{
    return x * x;
}

template <typename T>
void function(T (*func)(T), T a)
{
    cout << func(a) << " wynik funkcji przez argument" << endl;
}

int main()
{
    int a = 3;
    int *w_a = &a;                             // wszaznik na a, &a -> podaj adres a
    int &a_a = a;                              // refernecja na a
    cout << *w_a << " dziwna wartosc" << endl; // derefenrecja -> referencja z pointera

    int tab[5]; // tablica intów, która jest wskaźnikiem o długości 5;
    tab[0] = 123;
    cout << tab[0] << endl;
    int *tab2 = new int(50); // wskaźnik na 50 intów;
    tab2[1] = 666;
    delete[] tab2;
    cout << tab2[1] << " tab2" << endl;

    function(*myfunc<double>, 3.0);
    // * & tylko przed nazwą zmiennej ,nie po.

    ///// auto loop

    vector<int> vec = {1, 2, 3, 4};

    for (auto i : vec)
    {
        i += 1;
        cout << i << endl;
    }

    for (const int &i : vec)
    {
        cout << i << endl;
    }

    //// lambda

    //[przechwyt](deklaracja zmiennych np int a){co ma robic}(wartosci podane)

    double wynik = [](int a, int b) -> double
    { return a*1.0 * b / 2; }(a, a);
    cout << wynik+0.1 << " wynik tutaj" << endl;
    return 0;
}
