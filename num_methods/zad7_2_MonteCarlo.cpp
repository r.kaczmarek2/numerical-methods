#include <iostream>
#include <random>

using std::cout;
using std::endl;

int main()
{
    std::mt19937 gen{std::random_device{}()};
    std::uniform_real_distribution<double> losuj{-1., 1.};

    int punktow_w_kwadracie = 0;// Liczba punktow w kwadracie
    int punktow_w_kole = 0;     // Liczba punktow w kole


    punktow_w_kwadracie = 1000000;

    double x, y;
    for(int i{0}; i < punktow_w_kwadracie; ++i)
    {
        x = losuj(gen);
        y = losuj(gen);


        if(x*x + y*y <= 1)
        {
            ++punktow_w_kole;
        }
    }

    cout << "Liczba punktow w kole = " << punktow_w_kole << endl;
    cout << "Liczba punktow w kwadracie = " << punktow_w_kwadracie << endl;
    double _PI_ = 4. * punktow_w_kole / punktow_w_kwadracie;
    cout << "Szukana liczba pi = " << _PI_ << endl;
}