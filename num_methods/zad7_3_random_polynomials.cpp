// Zaimplementować funkcje generując N-wymiarowy wektor o losowych współczynnikach z podanych przedziałów.
#include <iostream>
#include <random>

using namespace std;

void Wypisz_wielomian(vector<double> v1, char v2)
{
    int a = v1.size() - 1;
    while (a > 0)
    {
        if (v1[a] != 0)
        {
            cout << v1[a] << ' ' << v2 << '^' << a << " + ";
        }

        a -= 1;
    }
    cout << v1[0] << endl;

    a += 1;
}

int main()
{
    mt19937 gen{random_device{}()};
    uniform_real_distribution<double> losuj{-10., 10.};

    int i, j, k;
    

    vector<int> ile_wsp = {5, 6, 2};
    vector<vector<double>> wsps;

    for (i = 0; i < ile_wsp.size(); i++)
    {
        vector<double> wsp(ile_wsp[i]);
        for (j = 0; j < ile_wsp[i]; j++)
        {
            wsp[j] = losuj(gen);
        }
        wsps.push_back(wsp);
    }

    vector<char> c = {'x', 'y', 'z'};
    for (i = 0; i < ile_wsp.size(); i++)
    {
        Wypisz_wielomian(wsps[i], c[i]);
    }
}
