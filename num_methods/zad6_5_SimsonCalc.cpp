//#include <matplot/matplot.h>
#include <vector>
#include <cmath>
#include <iostream>
#include <iostream>
#include <cmath>
#define M_PI 3.14159265358979323846
#include <numeric>

using namespace std;
//namespace plt = matplot;

vector<double> SimsonCalc(double a, double b, int n, double (*func)(double))
{
    double c = 0;
    double d = b / n;
    double temp = 0;
    vector<double> vec(n);

    for (int i = 0; i < n; i++)
    {
        temp = ((d - c) / 6) * (func(c) + 4 * func((c + d) / 2) + func(d));
        vec[i] = temp;
        c = d;
        d += b / n;
    }
    return vec;
}

int main()
{
    int n = 100;
    vector<double> wynik = SimsonCalc(0, M_PI * 2, n, sin);
    vector<double> x(100);

    for (int i = 0; i < n;i++)
    {
        x[i] = i*M_PI*2/n;
    }
    return 0;
}