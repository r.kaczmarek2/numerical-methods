// Zaimplementować funkcje generując N-wymiarowy wektor o losowych współczynnikach z podanych przedziałów.
#include <iostream>
#include <random>
#include <ctime>
#include <cmath>
#include <string>
#include <chrono>
using namespace std;

void Print_polynomial(vector<double> v1, string v2)
{
    int a = v1.size() - 1;
    while (a > 0)
    {
        if (v1[a] != 0)
        {
            cout << v1[a] << ' ' << v2 << '^' << a << " + ";
        }

        a -= 1;
    }
    cout << v1[0] << endl;

    a += 1;
}

double Calc_polynomial(double x, vector<double> v2)
{

    int a = 0;
    double wynik = 0;

    wynik = v2[0];
    a += 1;

    while (a < v2.size())
    {
        wynik += v2[a] * x;
        x *= x;
        a += 1;
    }
    return wynik;
}

vector<double> losowe_loc(vector<vector<double>> v1)
{
    vector<double> result(v1.size());

    auto seed = chrono::system_clock::now().time_since_epoch().count();

    static mt19937 gen(seed);

    for (int i = 0; i < v1.size(); i++)
    {
        uniform_real_distribution<double> losuj{v1[i][0], v1[i][1]};
        result[i] = losuj(gen);
    }
    return result;
}

/**
 * MC_integral
 * @param v1 - przedzialy do losowania
 * @param v2 - wspolczyniki
 * @param zmienne - nazwy zmiennych
 * @param n - liczba iteracji
 *
 * @return wynik calki
 */
double MC_integral(vector<vector<double>> v1, vector<vector<double>> wsps, vector<string> zmienne, int n = 1000)
{
    vector<double> loc;
    double finala_result = 0;
    double volume = 1;
    double temp_result = 0;
    if (v1.size() != wsps.size() || v1.size() != zmienne.size())
    {
        cout << "dlugosci wektorow sa rozne!!!" << endl;
        return 0;
    }

    for (int i = 0; i < v1.size(); i++)
    {
        volume *= fabs(v1[i][1] - v1[i][0]);
    }

    for (int k = 0; k < n; k++)
    {
        double temp = 0;
        loc = losowe_loc(v1); // x_0, x_1, x_2
        for (int i = 0; i < wsps.size(); i++)
        {
            temp += Calc_polynomial(loc[i], wsps[i]); // temp_result dla danych x_n, dla jednego n
        }
        temp_result += temp;
    }
    finala_result = volume * 1 / n * temp_result;
    return finala_result;
}
int main()
{

    int i, j, k;

    vector<vector<double>> intervals = {{0, 1}, {0, 2}, {-1, 2}};
    vector<vector<double>> coefficients = {{5, 1, 1}, {-4, -1, 2}, {5, 1, 3}};
    vector<string> variable_names = {" x ", " y ", " z "};

    double wynik = 0;
    double mean_wynik = 0;

    for (int i = 0; i < coefficients.size(); i++)
    {
        Print_polynomial(coefficients[i], variable_names[i]);
    }

    for (int i = 0; i < 1000; i++)
    {
        wynik = MC_integral(intervals, coefficients, variable_names);
        // cout << "wynik calki to " << wynik << endl;
        mean_wynik += wynik;
    }
    cout << "wynik usredniony to " << mean_wynik / 1000 << endl;
}