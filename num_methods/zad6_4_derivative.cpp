#include <iostream>
#include <cmath>

using namespace std;

double rozniczka(double x, double (*func)(double), double h = 0.0001)
{

    return (func(x + h) - func(x)) / h;
}

int main()
{
    double x = 0.5;
    cout << rozniczka(x, sin);
    return 0;
}