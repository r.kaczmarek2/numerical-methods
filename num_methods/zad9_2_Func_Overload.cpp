#include <iostream>
#include <cmath>
#include <vector>

namespace mynames
{
    double sin(double x)
    {
        return std::sin(x);
    }

    std::vector<double> sin(std::vector<double> x)
    {
        std::vector<double> result(x.size());
        for (int i = 0; i < x.size();)
        {
            result[i] = std::sin(x[i]);
        }
        return result;
    }

    double sin(double A, double x, double Ome, double fi)
    {
        return A * std::sin(Ome * x + fi);
    }

    std::vector<double> sin(std::vector<double> A, std::vector<double> x, std::vector<double> Ome, std::vector<double> fi)
    {
        std::vector<double> result(A.size());
        for (int i = 0; i < A.size(); i++)
        {
            result[i] = A[i] * std::sin(x[i] * Ome[i] + fi[i]);
        }
        return result;
    }
}

int main()
{
    double A = 1.3;
    std::cout << std::sin(3.3) << std::endl;
    std::vector<double> vec{A, 2, 3};
    std::cout << "wywolanie dla double" << std::endl;
    std::cout << mynames::sin(A) << std::endl;
    std::cout << mynames::sin(1.3, 2, 0.1, 0.2) << std::endl;

    return 0;
}