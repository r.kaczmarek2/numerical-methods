#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

vector<double> licz_funckje(vector<double>&vec ,double(*func)(double))
{
    vector<double> wynik(vec.size());
    for (int i = 0; i <vec.size(); i++)
    {
        wynik[i] = func(vec[i]);
    }
    return wynik;
}

    int main()
{
    vector<double> wynik = {1,2,3,4,5};
    wynik = licz_funckje(wynik, [](double x){return x*x;});
    for (int i = 0; i < wynik.size(); i++)
    {
        cout<<wynik[i]<<endl;
    }
    return 0;
}
