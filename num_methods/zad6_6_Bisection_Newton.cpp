#include <iostream>
#include <cmath>
#include <limits>
#include <string>
#include <iomanip>

using namespace std;

template <typename T>
T myfunc(T x)
{
    return (exp(-x) -x);
}

template <typename T>
T Bisection(T (*myfunc)(T), T a, T b, T epsilon)
{
    T liczba = 0;

    T x1 = (a + b) / 2;
    T f = myfunc(x1);
    if (f == 0)
    {
        return x1;
    }
    else
    {
        while (((fabs(myfunc(a) > epsilon*10000)) || (fabs(myfunc(b)> epsilon*10000))) && (liczba<100))
        {
            liczba += 1;
            if (myfunc(a) * myfunc(x1) < 0)
            {
                b = x1;
            }
            else if (myfunc(x1) * myfunc(b) < 0)
            {
                a = x1;
            }
            x1 = (a + b) / 2;
        }
        if (liczba==99)
        {
            cout<<"nie znaleziono wyniku w ramach iteracji"<<endl;
        }
        cout << "liczba iteracji " << liczba << ' ';
        return x1;
    }
}

template <typename T>
T Newton_0(T (*myfunc)(T), T x0, T epsilon, T h = 10e-6)
{
    T ilosc = 0;
    T x_1 = x0 - myfunc(x0) / ((myfunc(x0 + h) - myfunc(x0)) / h);
    T x = x_1 - myfunc(x_1) / ((myfunc(x_1 + h) - myfunc(x_1)) / h);
    while (fabs(x - x_1) > epsilon)
    {
        x_1 = x;
        x = x - myfunc(x) / ((myfunc(x + h) - myfunc(x)) / h);
        ilosc += 1;
    }
    cout << "liczba iteracji " << ilosc << ' ';
    return x;
}

int main()
{
    typedef numeric_limits<double> limD;
    double wynik = Bisection(myfunc, 0., 5., limD::epsilon());
    cout << setprecision(limD::max_digits10) << "Wynik = " << wynik << endl;

    typedef numeric_limits<double> limD;
    double wynik1 = Newton_0(myfunc, 25.55, limD::epsilon());
    cout << setprecision(limD::max_digits10) << "Wynik = " << wynik1 << endl;
    return 0;
}