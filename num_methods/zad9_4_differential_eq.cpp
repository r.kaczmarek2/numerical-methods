#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

vector<double> differential_eq(vector<double> f0, vector<double> params)
{
    vector<double> result(f0.size());

    result[0] = f0[1];
    result[1] = f0[2];
    result[2] = (-params[0] * f0[2] - params[1] * params[1] * f0[1]);

    return result;
}

vector<vector<double>> Diff_Euler(vector<double> f0, vector<double> params, vector<double> (*func)(vector<double>, vector<double>), float dt = 1e-4, int N = 1000)
{
    // Eulera method
    vector<vector<double>> result(N);
    vector<double> func_value;
    result[0] = f0;
    for (int i = 1; i < N; i++)
    {
        func_value = func(f0, params);
        for (int j = 0; j < f0.size(); j++)
        {
            f0[j] += dt * func_value[j];
        }
        result[i] = f0;
    }
    return result;
}

int main()

{
    vector<double> f0 = {5, 2, 3};
    vector<double> params = {0.002, 0.001};
    vector<double> wynik(3);

    vector<vector<double>> wynik_calosc;

    wynik = differential_eq(f0, params);

    wynik_calosc = Diff_Euler(f0, params, differential_eq);

    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            cout << wynik_calosc[i][j] << endl;
        }
        cout << endl;
    }
    return 0;
}