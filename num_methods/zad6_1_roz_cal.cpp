#include <matplot/matplot.h>
#include <vector>
#include <cmath>
#include <iostream>

using namespace std;
namespace plt = matplot;

vector<double> Fdifference(vector<double> v1, vector<double> v2)
{
	// v1 - wektor x, v2 - wektor y
	vector<double> v3(v1.size());
	v3[0] = (v2[1] - v2[0]) / (v1[1] + v1[0]);
	v3[v1.size() - 1] = (v2[v1.size() - 1] - v2[v1.size() - 2]) / (v1[v1.size() - 1] - v1[v1.size() - 2]);


	for (int i = 0; i < v1.size() - 2; i++)
	{
		v3[i+1] = (v2[i + 2] - v2[i]) / (v1[i + 2] - v1[i]);
	}
	return v3;
}

vector<double> Fintegral(vector<double> v1, vector<double> v2)
{
	// v1 - wektor x, v2 - wektor y, dx - róznica odległości w x, wynik = wartosc całki
	double dx = v1[1] - v1[0];
	vector<double> wynik(v1.size());
	wynik[0] = v2[0];
	double temp = 0;
	for (int i = 1; i < v1.size(); i++)
	{
		temp += (v2[i] + v2[i - 1]) * dx / 2;
		wynik[i] = temp;
	}
	return wynik;
}

int main()
{

	vector<double> x{ 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1. };
	vector<double> y{ 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1. };

	vector<double> z = Fdifference(x, y);
	vector<double> w = Fintegral(x, y);

	plt::plot(x, y);
	plt::hold(plt::on);
	plt::plot(x, z);
	plt::hold(plt::on);
	plt::plot(x, w);
	plt::show();

	return 0;
}