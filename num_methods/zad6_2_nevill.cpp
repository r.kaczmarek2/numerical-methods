//#include <matplot/matplot.h>
#include <vector>
#include <cmath>
#include <iostream>

using namespace std;
// namespace plt = matplot;

vector<double> Neville(vector<double> x, vector<double> y, double X)
{
    vector<double> temp(y);
    int iter = 0;
    while (temp.size() > 1)
    {
        vector<double> obecny;

        for (int i = 0; i < temp.size() - 1; i++)
        {
            obecny.push_back((((X - x[i]) * temp[i + 1]) - (X - x[i + 1 + iter]) * temp[i]) / (x[i + 1 + iter] - x[i]));
        }
        temp = obecny;
        iter += 1;
    }
    return temp;
}
int main()
{
    vector<double> x = {2, 3, 6, 8, 10};
    vector<double>xx ={10,15,17,20,25,30};
    vector<double> y = {4, 9, 36, 64, 100};
    vector<double> z(5);
    vector<double> wynik = Neville(x, y, 7);

    for (int i = 0; i < 5; i++)
    {
        wynik =  Neville(x,y,x[i]);
        z[i] = wynik[0];
    }
    
    return 0;
}
