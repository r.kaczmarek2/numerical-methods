#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

void Wypisz_wielomian(vector<double> v1, char v2)
{
    int a = v1.size() - 1;
    while (a > 0)
    {
        if (v1[a] != 0)
        {
            cout << v1[a] << ' ' << v2 << '^' << a << " + ";
        }

        a -= 1;
    }
    cout << v1[0] << endl;

    a += 1;
}

double Licz_wielomian(double x, vector<double> v1)
{
    // Wypisz_wielomian(v1, v2);
    //  vector wspolczynnikwo (0 to  0 stopnia etc)
    int a = 0;
    double wynik = 0;

    wynik = v1[0];
    a += 1;

    while (a < v1.size())
    {
        wynik += v1[a] * x;
        x *= x;
        a += 1;
    }
    return wynik;
}

double Iloczyn_Wielomianow(vector<double> pos, vector<vector<double>> v1)
{
    double wynik = 1;
    vector<double> vec_wynik(pos.size());

    for (int i = 0; i < v1.size(); i++)
    {
        wynik *= Licz_wielomian(pos[i], v1[i]);
    }
    return wynik;
}

int main()
{
    int x = 2;
    vector<double> wsp = {1, 2};
    Wypisz_wielomian(wsp, 'x');
    double wynik = Licz_wielomian(x, wsp);
    cout << "wartosc w punkcie x = " << x << " to " << wynik << endl;

    x = 2;
    wsp = {3, 2};
    Wypisz_wielomian(wsp, 'x');
    wynik = Licz_wielomian(x, wsp);
    cout << "wartosc w punkcie x = " << x << " to " << wynik << endl;

    cout << "Mnozenie wielomianow" << endl;
    vector<double> pos = {2, 1, 1};
    vector<vector<double>> wsps = {{1, 1, 1}, {1, 1}, {0, 0, 0, 2}};

    Wypisz_wielomian(wsps[0], 'x');
    Wypisz_wielomian(wsps[1], 'y');
    Wypisz_wielomian(wsps[2], 'z');

    wynik = Iloczyn_Wielomianow(pos, wsps);

    cout << "wynik mnozenia wielomianow to " << wynik << endl;

    return 0;
}