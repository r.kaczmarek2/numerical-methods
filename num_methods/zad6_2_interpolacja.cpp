//#include <matplot/matplot.h>
#include <vector>
#include <cmath>
#include <iostream>

using namespace std;
// namespace plt = matplot;

vector<double> Interpolate(vector<double> x, vector<double> y, vector<double> x_0)
{
    vector<double> Lx(x_0.size());
    vector<double> l(x.size());
    for (int n = 0; n < x_0.size(); n++)
    {
        for (int i = 0; i < x.size(); i++)
        {
            l[i] = 1;
            for (int j = 0; j < x.size(); j++)
            {
                if (i == j)
                {
                    continue;
                }
                else
                {
                    l[i] *= (x_0[n] - x[j]) / (x[i] - x[j]);
                }
            }
            Lx[n] += y[i] * l[i];
        }
    }

    return Lx;
}

int main()
{
    vector<double> x = {2, 3, 6, 8, 10};
    vector<double> y = {4, 9, 36,64,100};
    vector<double> z = {1, 2.5, 3.5, 5,7.8,7.6, 12};
    vector<double> wynik = Interpolate(x, y, z);

    cout << "Wynik interpolacji dla zadanych punktow" << endl;
    for (int i = 0; i < wynik.size(); i++)
    {
        cout << "zadane x = " << z[i] << " wynik interpolacji " << wynik[i] << endl;
    }
    return 0;
}
