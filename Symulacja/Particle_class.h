#ifndef __Particle__
#define __Particle__

#include <iostream>
#include <string>

class Particle
{
public:
    double x_pos;
    double y_pos;
    double x_vel;
    double y_vel;
    double x_acc;
    double y_acc;
    double mass;
    double size;
    int ids;

    // Konstruktory
    Particle()
    {
        x_pos = 0;
        y_pos = 0;
        x_vel = 0;
        y_vel = 0;
        x_acc = 0;
        y_acc = 0;
        mass = 0;
        size = 0;
        ids = 0;
    }

    Particle(double _x_pos, double _y_pos, double _x_vel, double _y_vel, double _x_acc, double _y_acc, double _mass, double _size);
    Particle operator=(const Particle &p2);
    bool operator>(const Particle &p2);
    bool operator<(const Particle &p2);
    friend std::ostream &operator<<(std::ostream &ostream, Particle &Z)
    {
        ostream << "[ x " << Z.x_pos << " y " << Z.y_pos << "] Pozycja " << std::endl
                << "[ x " << Z.x_vel << " y " << Z.y_vel << "] Predkosc " << std::endl
                << "[ x " << Z.x_acc << " y " << Z.y_acc << "] Przyspieszenie " << std::endl
                << "[Mass " << Z.mass << " Size " << Z.size << ']' << std::endl
                << "ids = " << Z.ids << std::endl;
        return ostream;
    }
};

#include "Particle_class.C"
#endif