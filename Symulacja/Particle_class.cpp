#include <iostream>
#include <string>
#include "Particle_class.h"

int main()
{
    Particle A;
    Particle B(-1, 2.5, 3, 4, 5, 6, 10, 1);
    bool c = B > A;

    std::cout << A << std::endl;
    std::cout << B << std::endl;
    std::cout <<"wartosc bool "<<c << std::endl;
    return 0;
}