#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <random>
#include <algorithm>
#include <chrono>
#include <functional>

#include "Particle_class.h"

class Simulation
{
public:
    std::vector<std::vector<double>> draw;
    std::vector<Particle> particles;
    double x_bounds, y_bounds;
    int frame;

    Simulation()
    {
        draw = std::vector<std::vector<double>>(1, std::vector<double>(4));
        particles = std::vector<Particle>(1, Particle());
        x_bounds = 10;
        y_bounds = 10;
        frame = 0;
    }

    Simulation(double _x_bounds, double _y_bounds, int n_particle)
    {
        x_bounds = _x_bounds;
        y_bounds = _y_bounds;
        draw = std::vector<std::vector<double>>(4, std::vector<double>(n_particle, 0));
        particles = std::vector<Particle>(n_particle, Particle());
        frame = 0;
    }

    void Add_particle(double m, double s, int n_particle)
    {
        auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
        std::mt19937 gen(seed);
        std::uniform_real_distribution<double> losuj_pos_x{0., x_bounds};
        std::uniform_real_distribution<double> losuj_pos_y{0., y_bounds};
        std::uniform_real_distribution<double> losuj_vel_x{-5., 5.};
        std::uniform_real_distribution<double> losuj_vel_y{-5., 5.};

        for (int i = 0; i < n_particle; i++)
        {
            particles[i].x_pos = losuj_pos_x(gen);
            particles[i].y_pos = losuj_pos_y(gen);
            particles[i].x_vel = losuj_vel_x(gen);
            particles[i].y_vel = losuj_vel_y(gen);
            particles[i].x_acc = 0;
            particles[i].y_acc = 0;
            particles[i].mass = m;
            particles[i].size = s;
            particles[i].ids = i;
        }
    }
    void Draw_particle()
    {
        // update draw
        for (int i = 0; i < particles.size(); i++)
        {
            draw[0][i] = particles[i].x_pos;
            draw[1][i] = particles[i].y_pos;
            draw[2][i] = 0;
            draw[3][i] = std::sqrt(particles[i].x_vel * particles[i].x_vel + particles[i].y_vel * particles[i].y_vel);
        }
    }

    void Update()
    {
        double dt = 1;
        frame += 1;
        for (int i = 0; i < particles.size(); i++)
        {

            particles[i].x_pos += dt * particles[i].x_vel;
            particles[i].y_pos += dt * particles[i].y_vel;
        }
    }

    void Collision()
    {
        std::sort(particles.begin(), particles.end());
        std::vector<int> poss_ids;

        for (int i = 0; i < particles.size(); i++)
        {
            if ((particles[i + 1].x_pos - particles[i + 1].size) - (particles[i].x_pos + particles[i].size) < 0)
            {
                if (std::sqrt((particles[i + 1].x_pos - particles[i].x_pos) * (particles[i + 1].x_pos - particles[i].x_pos) + (particles[i + 1].y_pos - particles[i].y_pos) * (particles[i + 1].y_pos - particles[i].y_pos)) < particles[i + 1].size + particles[i].size)
                    ;
                {
                    double dot1 = 0, dot2 = 0;
                    std::vector<double> wynik_1(2);
                    std::vector<double> wynik_2(2);

                    std::vector<double> v1_r = {particles[i].x_vel - particles[i + 1].x_vel, particles[i].y_vel - particles[i + 1].y_vel};
                    std::vector<double> v2_r = {particles[i + 1].x_vel - particles[i].x_vel, particles[i + 1].y_vel - particles[i].y_vel};

                    std::vector<double> x1_r{particles[i].x_pos - particles[i + 1].x_pos, particles[i].y_pos - particles[i + 1].y_pos};
                    std::vector<double> x2_r{particles[i + 1].x_pos - particles[i].x_pos, particles[i + 1].y_pos - particles[i].y_pos};

                    for (int q = 0; q < 2; q++)
                    {
                        dot1 += v1_r[q] * x1_r[q];
                        dot2 += v2_r[q] * x2_r[q];
                    }

                    wynik_1[0] = particles[i].x_vel - ((2*particles[i+1].mass)/(particles[i+1].mass + particles[i].mass)) * dot1 / (x1_r[0]*x1_r[0] + x1_r[1] *x1_r[1]) * x1_r[0];
                    wynik_1[1] = particles[i].y_vel - ((2*particles[i+1].mass)/(particles[i+1].mass + particles[i].mass)) * dot1 / (x1_r[0]*x1_r[0] + x1_r[1] *x1_r[1]) * x1_r[1];

                    wynik_2[0] = particles[i+1].x_vel - ((2*particles[i].mass)/(particles[i+1].mass + particles[i].mass)) * dot2 / (x2_r[0]*x2_r[0] + x2_r[1] *x2_r[1]) * x2_r[0];
                    wynik_2[1] = particles[i+1].x_vel - ((2*particles[i].mass)/(particles[i+1].mass + particles[i].mass)) * dot2 / (x2_r[0]*x2_r[0] + x2_r[1] *x2_r[1]) * x2_r[1];

                    particles[i].x_vel = wynik_1[0];
                    particles[i].y_vel = wynik_1[1];
                    particles[i+1].x_vel = wynik_2[0];
                    particles[i+1].y_vel = wynik_2[1];
                }
            }

            // sprawdz czy na terenie
            if (particles[i].x_pos < particles[i].size)
            {
                particles[i].x_vel = -particles[i].x_vel;
            }
            if (particles[i].x_pos > x_bounds - particles[i].size)
            {
                particles[i].x_vel = -particles[i].x_vel;
            }
            if (particles[i].y_pos < particles[i].size)
            {
                particles[i].y_vel = -particles[i].y_vel;
            }
            if (particles[i].y_pos > y_bounds - particles[i].size)
            {
                particles[i].y_vel = -particles[i].y_vel;
            }
        }
    }
};

int main()
{
    int n_particle = 1;

    Simulation sim(100, 100, n_particle);
    sim.Add_particle(1, 2, n_particle);
    sim.particles[0].x_pos = 3;
    sim.particles[0].x_vel = -0.3;
    for (int i = 0; i < 10; i++)
    {
        sim.Draw_particle();
        sim.Collision();
        sim.Update();

        std::cout << sim.particles[0];
    }
    return 0;
}